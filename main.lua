require 'src/rect'
require 'src/play_area'
require 'src/paddle'
require 'src/ball'
require 'src/ai'

-------------------------------------------------------------------------------
-- Game state
-------------------------------------------------------------------------------

local PADDLE_DISTANCE_FROM_WALLS = 60

local NET_SECTION_HEIGHT = 40
local NET_SECTION_DISTANCE = 80
local NET_WIDTH = 10

local font

local playArea
local paddle1
local paddle2
local ball
local ai

local player1Score = 0
local player2Score = 0

-------------------------------------------------------------------------------
-- Internal logic functions
-------------------------------------------------------------------------------

local function processUserInput(dt)
  if love.keyboard.isDown("down") then
    paddle1:moveDown(dt)   
  elseif love.keyboard.isDown("up") then
    paddle1:moveUp(dt)
  end
end

local function updateBall(dt)
  ball:checkPaddleCollide(paddle1)
  ball:checkPaddleCollide(paddle2)

  local hitSide, side = ball:update(dt)
  if hitSide then
    ball:reset()
    if side == PlayArea.LEFT_SIDE then
      player2Score = player2Score + 1
    elseif side == PlayArea.RIGHT_SIDE then
      player1Score = player1Score + 1
    end
  end
end

-------------------------------------------------------------------------------
-- Main Love2D callbacks
-------------------------------------------------------------------------------

function love.load()
  font = love.graphics.newFont(20)

  local wWidth, wHeight = love.graphics.getWidth(), love.graphics.getHeight()
  playArea = PlayArea:new(wWidth, wHeight)
  paddle1 = Paddle:new(playArea, PADDLE_DISTANCE_FROM_WALLS, wHeight / 2)
  paddle2 = Paddle:new(playArea, 
                       wWidth - PADDLE_DISTANCE_FROM_WALLS, wHeight / 2)
  ball = Ball:new(playArea)
  ai = AI:new(paddle2, ball)
end

function love.draw()
  love.graphics.setBackgroundColor(0, 0, 0, 255)

  -- Draw the paddles
  paddle1:draw()
  paddle2:draw()

  -- Draw the net
  local netX = (playArea.width / 2) - (NET_WIDTH / 2)
  for y = 0, playArea.height, NET_SECTION_DISTANCE do
    love.graphics.setColor(255, 255, 255, 255, 255)
    love.graphics.rectangle("fill", netX, y, NET_WIDTH, NET_SECTION_HEIGHT)
  end

  -- Draw the ball
  ball:draw()

  -- Draw the score
  local wWidth = love.graphics.getWidth()
  local player1Width = font:getWidth(tostring(player1Score))
  love.graphics.setFont(font)
  love.graphics.print(tostring(player1Score), (wWidth / 2) - 60 - player1Width, 10)
  love.graphics.print(tostring(player2Score), (wWidth / 2) + 60, 10)
end

function love.update(dt)
  processUserInput(dt)
  ai:update(dt)
  updateBall(dt)
end

function love.keypressed(key)
  if key == "escape" then
    love.event.push("quit")
  end
end

