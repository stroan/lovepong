-------------------------------------------------------------------------------
-- Play area definitions
-------------------------------------------------------------------------------

PlayArea = {LEFT_SIDE = 0,
            RIGHT_SIDE = 1}
PlayArea.__index = PlayArea
function PlayArea:new(width, height)
  local o = {width = width, height = height}
  setmetatable(o, self)
  return o
end

function PlayArea:constrainY(top, height)
  if height > self.height then
    return false, top
  elseif top < 0 then
    return true, 0
  elseif top + height > self.height then
    return true, self.height - height
  else
    return false, top
  end
end

function PlayArea:isInX(left, width)
  if width > self.width then
    return false
  elseif left < 0 then
    return true, PlayArea.LEFT_SIDE
  elseif left + width > self.width then
    return true, PlayArea.RIGHT_SIDE
  else
    return false
  end
end


