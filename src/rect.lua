-------------------------------------------------------------------------------
-- Rect definitions
-------------------------------------------------------------------------------
Rect = {}
Rect.__index = Rect
function Rect:new(x, y, width, height)
  local o = {x = x,
             y = y,
	     width = width,
	     height = height}
  setmetatable(o, self)
  return o
end

function Rect:intersects(other)
  local xs = self.x > other.x + other.width or self.x + self.width < other.x
  local ys = self.y > other.y + other.height or self.y + self.height < other.y
  return not (xs or ys)
end

function Rect:getCenter()
  return self.x + (self.width / 2), self.y + (self.height / 2)
end


