-------------------------------------------------------------------------------
-- AI
-------------------------------------------------------------------------------

local AI_FUDGE = 10

AI = {}
AI.__index = AI
function AI:new(paddle, ball)
  local o = {paddle = paddle,
             ball = ball}
  setmetatable(o, self)
  return o
end

function AI:update(dt)
  if self.ball.vx < 0 then
    return
  end

  local _, selfy = self.paddle:getBounds():getCenter()
  local _, bally = self.ball:getBounds():getCenter()

  if bally < selfy - AI_FUDGE then
    self.paddle:moveUp(dt)
  elseif bally > selfy + AI_FUDGE then
    self.paddle:moveDown(dt)
  end
end


