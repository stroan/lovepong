-------------------------------------------------------------------------------
-- Ball definitions
-------------------------------------------------------------------------------

local BALL_SIZE = 20
local BALL_SPEED = 400

Ball = {}
Ball.__index = Ball
function Ball:new(playArea)
  local o = {playArea = playArea}
  setmetatable(o, self)
  o:reset()
  return o
end

function Ball:reset()
  self.x = (self.playArea.width / 2) - (BALL_SIZE / 2)
  self.y = (self.playArea.height / 2) - (BALL_SIZE / 2)

  -- 90 degree intiial range, pointing at plater 2
  local direction = (math.pi / 4) - (math.random() * (math.pi / 2))
  self.vx = math.cos(direction) * BALL_SPEED
  self.vy = math.sin(direction) * BALL_SPEED
end

function Ball:draw()
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.rectangle("fill", self.x, self.y, BALL_SIZE, BALL_SIZE)
end

function Ball:update(dt)
  self.x = self.x + (self.vx * dt)
  self.y = self.y + (self.vy * dt)

  local needsBounce, newY = self.playArea:constrainY(self.y, BALL_SIZE)
  if needsBounce then
    self.vy = - self.vy
    self.y = newY
  end

  local hitSide, player = self.playArea:isInX(self.x, BALL_SIZE)
  return hitSide, player
end

function Ball:getBounds()
  return Rect:new(self.x, self.y, BALL_SIZE, BALL_SIZE)
end

function Ball:checkPaddleCollide(paddle)
  local paddleBounds = paddle:getBounds()
  local selfBounds = self:getBounds()
  if selfBounds:intersects(paddleBounds) then
    local px, py = paddleBounds:getCenter()
    local sx, sy = selfBounds:getCenter()

    local vx, vy = sx - px, sy - py
    local vMag = math.sqrt((vx ^ 2) + (vy ^ 2))

    self.vx = (vx / vMag) * BALL_SPEED
    self.vy = (vy / vMag) * BALL_SPEED

    if sx > px then
      self.x = paddleBounds.x + paddleBounds.width
    else
      self.x = paddleBounds.x - BALL_SIZE
    end
  end
end


