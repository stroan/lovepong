-------------------------------------------------------------------------------
-- Paddle definitions
-------------------------------------------------------------------------------

local PADDLE_HEIGHT = 50
local PADDLE_WIDTH = 25
local PADDLE_SPEED = 300

Paddle = {}
Paddle.__index = Paddle
function Paddle:new(playArea, x, y) 
  local o = {playArea = playArea,
	     x = x - (PADDLE_WIDTH / 2), 
             y = y - (PADDLE_HEIGHT / 2)}
  setmetatable(o, self)
  o:ensureInPlayArea()
  return o
end

function Paddle:moveDown(dt)
  self.y = self.y + (PADDLE_SPEED * dt)
  self:ensureInPlayArea()
end

function Paddle:moveUp(dt, speed)
  self.y = self.y - (PADDLE_SPEED * dt)
  self:ensureInPlayArea()
end

function Paddle:ensureInPlayArea()
  local _, newTop = self.playArea:constrainY(self.y, PADDLE_HEIGHT)
  self.y = newTop
end

function Paddle:draw()
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.rectangle("fill", self.x, self.y, PADDLE_WIDTH, PADDLE_HEIGHT)
end

function Paddle:getBounds()
  return Rect:new(self.x, self.y, PADDLE_WIDTH, PADDLE_HEIGHT)
end


